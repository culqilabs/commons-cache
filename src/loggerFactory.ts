import { LoggerFactory } from "@culqi/bootstrap";
import config from "./config";

export default new LoggerFactory(config);
