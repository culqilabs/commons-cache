import httpStatus from "http-status-codes";
import * as dataAccess from "./data-access";
import { listCommons } from "../providers/common";
import loggerFactory from "../loggerFactory";
import { GetCommonBySlug, GetCommonByAlias, GetCommonByName } from "../types/commons-cache";

const logger = loggerFactory.getLogger("app");

export const getByAlias = async (data: GetCommonByAlias) => {
  const {
    collection,
    detail: { alias },
  } = data;
  const record = await dataAccess.readOne(collection, { alias });
  if (record) return { data: record };
  return {
    error: {
      message: "generic.errors.recordNotFound",
      status: httpStatus.NOT_FOUND,
    },
  };
};

export const getByName = async (data: GetCommonByName) => {
  const {
    collection,
    detail: { name },
  } = data;
  const record = await dataAccess.readOne(collection, { name });
  if (record) return { data: record };
  return {
    error: {
      message: "generic.errors.recordNotFound",
      status: httpStatus.NOT_FOUND,
    },
  };
};

export const getBySlug = async (data: GetCommonBySlug) => {
  const {
    collection,
    detail: { slug },
  } = data;
  const record = await dataAccess.readOne(collection, { slug });
  if (record) return { data: record };
  return {
    error: {
      message: "generic.errors.recordNotFound",
      status: httpStatus.NOT_FOUND,
    },
  };
};

export const list = async (collection: string) => {
  const records = await dataAccess.read(collection, {});
  if (records && records.length >= 0) return { data: records };
  return {
    error: {
      message: "generic.errors.emptyList",
      status: httpStatus.NOT_FOUND,
    },
  };
};

export const reload = async (collection: string) => {
  const request = {
    collection,
    detail: { isDeleted: false, status: "enabled" },
  };
  const { data: commonList, error } = await listCommons(request);
  if (error) return null;
  const recordsReload = await dataAccess.create(collection, commonList);
  return recordsReload;
};

export const reloadDefaultCollections = async (defaultCollections: string[]) => {
  let result = false;
  try {
    for (const collectionName of defaultCollections) {
      result = false;
      const request = {
        collection: collectionName,
        detail: { isDeleted: false, status: "enabled" },
      };
      const { data: commonList, error } = await listCommons(request);
      if (error || (commonList && commonList.length === 0)) {
        logger.error(`Error reloading ${collectionName}, or is empty`);
        logger.error(error);
        return result;
      }
      await dataAccess.create(collectionName, commonList);
      logger.info(`Succesfully reloaded ${collectionName}`);
      result = true;
    }
  } catch (err) {
    logger.error(err);
    return false;
  }
  return result;
};
