import { Queue } from "@culqi/bootstrap";
import { Consumer, Message } from "kafkajs";
import loggerFactory from "../loggerFactory";
import { reload } from "./common";

const logger = loggerFactory.getLogger("app");

const handler = {
  eachMessage: async ({ message: { value } }: { message: Message }) => {
    try {
      const parseMessage = JSON.parse(value.toString());
      const {
        event,
        content: {
          data: { collection },
        },
      } = parseMessage;
      try {
        if (event === "commonsUpdate") {
          if (!(await reload(collection))) throw new Error("Error on data reload");
        }
      } catch (err) {
        logger.error("Error when reloading commons");
        logger.error(err);
        process.exit(1);
      }
    } catch (e) {
      logger.error("Error when consuming Kafka queue");
      logger.error(e.message);
    }
  },
};

const queueHandler = async ({ event, content: { data } }: { event: string; content: { data: any } }) => {
  if (event === "commonsUpdate") {
    try {
      const { collection } = data;
      if (!(await reload(collection))) throw new Error("Error on data reload");
    } catch (err) {
      logger.error("Error when reloading commons");
      logger.error(err);
      process.exit(1);
    }
  }
};

export default async (consumer: Consumer | Queue) => {
  if (consumer instanceof Queue) {
    consumer.on("events", queueHandler);
  } else {
    await consumer.run(handler);
  }
};
