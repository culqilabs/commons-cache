import Loki from "lokijs";
import config from "../helpers/params";

const databaseName = config.cacheDb.name;
const lokiDb = new Loki(databaseName);

export const readOne = async (collectionName: string, filter: any) => {
  const collection = await lokiDb.getCollection(collectionName);
  if (collection) return await collection.findOne(filter);
};

export const read = async (collectionName: string, filter: any) => {
  const collection = await lokiDb.getCollection(collectionName);
  if (collection) return await collection.find(filter);
};

export const create = async (collectionName: string, data: any) => {
  await lokiDb.removeCollection(collectionName);
  const collection = await lokiDb.addCollection(collectionName);
  return await collection.insert(data);
};
