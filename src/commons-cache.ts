import { Config, Events, HealthCheck, LoggerFactory, Queue } from "@culqi/bootstrap";
import { getByAlias, getByName, getBySlug, list, reloadDefaultCollections } from "./services/common";
import consumer from "./services/consumer";
import { GetCommonByAlias, GetCommonByName, GetCommonBySlug } from "./types/commons-cache";

export class CommonsCache {
  public getBySlug: (data: GetCommonBySlug) => Promise<{ data: any; error?: undefined } | { error: { message: string; status: number }; data?: undefined }>;
  public list: (collection: string) => Promise<{ data: any[]; error?: undefined } | { error: { message: string; status: number }; data?: undefined }>;
  public getByAlias: (data: GetCommonByAlias) => Promise<{ data: any; error?: undefined } | { error: { message: string; status: number }; data?: undefined }>;
  public getByName: (data: GetCommonByName) => Promise<{ data: any; error?: undefined } | { error: { message: string; status: number }; data?: undefined }>;

  constructor(private config: Config, private loggerFactory: LoggerFactory, private events: Events | Queue, private healthCheck?: HealthCheck) {
    const collections = this.config.me.collections.names;
    const logger = this.loggerFactory.getLogger("startup");
    if (collections) {
      logger.info("preparing commons cache");
      this.getBySlug = getBySlug;
      this.getByName = getByName;
      this.getByAlias = getByAlias;
      this.list = list;

      reloadDefaultCollections(collections).then((initialReload) => {
        if (!initialReload) {
          logger.error("ERROR on initial reload");
          setTimeout(() => process.exit(1), 1000);
        }
        consumer(events instanceof Queue ? events : events.kafkaConsumer).then(
          () => {
            logger.info("connected to queue");
          },
          () => {
            logger.error("error on queue connection");
            setTimeout(() => process.exit(1), 1000);
          },
        );
      });
    }
  }
}
