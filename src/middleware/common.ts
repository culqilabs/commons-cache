import { Request, Response, NextFunction } from "express";
import httpStatus from "http-status-codes";
import BaseJoi from "@hapi/joi";
import Extension from "@hapi/joi-date";

const Joi = BaseJoi.extend(Extension);

const getSchema = Joi.object().keys({
  collection: Joi.string()
    .trim()
    .required(),
  detail: Joi.object()
    .required()
    .keys({
      slug: Joi.string()
        .trim()
        .required(),
    }),
});

const listSchema = Joi.object().keys({
  collection: Joi.string()
    .trim()
    .required(),
});

const buildMiddleware = (schema: any) => async (req: Request, res: Response, next: NextFunction) => {
  const { meta, data } = res.locals;
  const { error } = schema.validate(data);
  if (error) {
    return res.$businessError({
      message: error.message,
      status: httpStatus.BAD_REQUEST,
    });
  }
  return next();
};

export default {
  schemas: {
    get: buildMiddleware(getSchema),
    list: buildMiddleware(listSchema),
  },
};
