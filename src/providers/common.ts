import axios from "axios";
import config from "../helpers/params";
import loggerFactory from "../loggerFactory";

const logger = loggerFactory.getLogger("app");

const commonsConfig = config.providers.commons;
logger.info(`loading master-data from: ${commonsConfig.url} `);

const api = axios.create({
  baseURL: commonsConfig.url,
  headers: {
    "Content-Type": "application/json",
  },
  responseType: "json",
});

//TODO: pendiente validar si existe algo en culqiLabs para esto
const requestGenerator = () => {
  const requestData = {
    meta: {
      timestamp: "2019-11-18T15:16:39.229Z-0500",
      serviceId: "ms-commons",
      request: {
        traceId: "234567d8s9f00a9s8df6sa8fasfa97a8d7f",
        ipAddress: "127.0.0.1",
        userId: "user_0003",
      },
    },
    data: {},
  };
  return requestData;
};

export const listCommons = async (commonData: any) => {
  const commonRequest = requestGenerator();
  commonRequest.data = commonData;
  const { data } = await api.post("/list", commonRequest);
  return data;
};
