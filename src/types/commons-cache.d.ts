export type GetCommonBySlug = {
  collection: string,
  detail: {
    slug: string
  }
}

export type GetCommonByName = {
  collection: string,
  detail: {
    name: string
  }
}

export type GetCommonByAlias = {
  collection: string,
  detail: {
    alias: string
  }
}
