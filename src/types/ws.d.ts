declare namespace Express {
  export type $BusinessError = {
    message: string
    status: number
  }
  export type $InternalError = {
    message: string
    status: 511 | 507 | 505 | 504 | 503 | 502 | 501 | 500 | 431 | 429 | 428 | 424 | 423 | 422 | 420 | 419 | 418 | 417 | 416 | 415 | 414 | 413 | 412 | 411 | 410 | 409 | 408 | 407 | 406 | 405 | 404 | 403 | 402 | 401 | 400
  }
  export interface Response {
    $t: (phrase: string, replacements?: { [key: string]: string }) => string
    $tn: (phrase: string, replacements?: { [key: string]: string }) => string
    $success: (data: any) => void
    $businessError: (error: $BusinessError) => void
    $internalError: (error: $InternalError) => void
    $logTrace: (message: any) => void
    $logDebug: (message: any) => void
    $logInfo: (message: any) => void
    $logWarn: (message: any) => void
    $logError: (message: any) => void
    $logFatal: (message: any) => void
  }
}
