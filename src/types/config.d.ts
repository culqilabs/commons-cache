import log4js from "log4js";

export type HealthCheck = {
  signal: string
  timeout: number
}

export type Application = {
  serviceId?: string
  timezone?: string
  dateFormat?: string
  port?: number
}

export type Config = {
  application: Application
  log4js: log4js.Configuration
  mongoose?: {
    [name: string]: Mongoose
  }
  ioredis?: {
    [name: string]: Ioredis
  }
  healthCheck: HealthCheck
}

export type MongooseOptions = {
  sslCA?: string[]
  [name: string]: any
}

export type Mongoose = {
  uri: string
  options: MongooseOptions
}

export type IoredisOptions = {
  [name: string]: any
}

export type Ioredis = {
  uri: string
  options: IoredisOptions
}
