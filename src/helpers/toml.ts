import toml from "@iarna/toml";
import fs from "fs";
import path from "path";

const TOML_EXTENSION = ".toml";

const parse = (file: string, encoding = "utf8"): toml.JsonMap => {
  return toml.parse(fs.readFileSync(file, encoding));
};

export default (dir: string) => {
  const map = new Map<string, () => toml.JsonMap>();
  fs.readdirSync(dir)
    .filter((file) => file.endsWith(TOML_EXTENSION))
    .forEach((file) => map.set(path.basename(file, TOML_EXTENSION), () => parse(path.join(dir, file))));
  return (name: string, defaultConfig = "local"): any => (map.has(name) ? map.get(name)() : map.get(defaultConfig)());
};
