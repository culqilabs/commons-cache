const localConfig = Object.freeze({
  cacheDb: {
    name: "local-commons-db",
  },
  providers: {
    commons: {
      url: "http://localhost:3030/commons/internal",
    },
  },
});

const developConfig = Object.freeze({
  cacheDb: {
    name: "local-commons-db",
  },
  providers: {
    commons: {
      url: "http://ms-commons:3000/commons/internal",
    },
  },
});

const qaConfig = Object.freeze({
  cacheDb: {
    name: "local-commons-db",
  },
  providers: {
    commons: {
      url: "http://ms-commons:3000/commons/internal",
    },
  },
});

const prodConfig = Object.freeze({
  cacheDb: {
    name: "local-commons-db",
  },
  providers: {
    commons: {
      url: "http://ms-commons:3000/commons/internal",
    },
  },
});

const testConfig = Object.freeze({
  cacheDb: {
    name: "jest",
  },
  providers: {
    commons: {
      url: "http://localhost:3030/commons/internal",
    },
  },
});

const map = new Map();
map.set("test", testConfig);
map.set("local", localConfig);
map.set("development", developConfig);
map.set("qa", qaConfig);
map.set("production", prodConfig);

export default map.get(process.env.NODE_ENV);
