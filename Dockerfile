FROM node:10

# create app directory
RUN mkdir -p /app
WORKDIR /app

# install app dependencies
COPY package*.json /app/
RUN npm install

# Bundle app source
COPY . /app
RUN npm run build-ts

EXPOSE 3000

ENTRYPOINT npm start
