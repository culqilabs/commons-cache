import * as dataAccess from "../../src/services/data-access";
import { BanksCollection, updBanksCollection } from "./commonsData";

describe("data-access-test", () => {
  describe("create", () => {
    let records: [any];
    let updatedRecords: [any];
    it("should create (initialize) a cached database", async () => {
      records = await dataAccess.create("test-collection", BanksCollection);
      expect(records).toBeTruthy();
    });
    it("number of records should be the same in cache and in source db", async () => {
      expect(records.length).toEqual(BanksCollection.length);
    });

    it("should allow to reload-update a collection", async () => {
      updatedRecords = await dataAccess.create("test-collection", updBanksCollection);
      expect(records).toBeTruthy();
    });

    it("updated records should have consistent length", async () => {
      expect(updatedRecords.length).toEqual(updBanksCollection.length);
    });
  });

  describe("readOne", () => {
    let common: any;
    it("should allow getting a single value from cached database", async () => {
      common = await dataAccess.readOne("test-collection", { slug: "zBjED2mhgU8bRiQ0" });
      expect(common).toBeTruthy();
      expect(common).not.toHaveProperty("length");
    });

    it("should allow querying an inexistant value for falsy", async () => {
      const emptyCommon = await dataAccess.readOne("test-collection", { slug: "inexistant" });
      expect(emptyCommon).toBeFalsy();
    });
  });

  describe("read", () => {
    let commonList;
    it("should allow getting collections from cached database", async () => {
      commonList = await dataAccess.read("test-collection", {});
      expect(commonList).toBeTruthy();
      expect(commonList).toHaveProperty("length");
    });

    it("should allow to query for inexistant collections and return falsy", async () => {
      const falsyCommonList = await dataAccess.read("inexistant-collection", {});
      expect(falsyCommonList).toBeFalsy();
    });
  });
});
