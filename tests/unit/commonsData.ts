import ISODate from "isodate";

export const BanksCollection = [
  {
    _id: "5de931dd53674b6086cb80f1",
    isDeleted: false,
    alias: "bcp",
    name: "Banco de Crédito del Perú",
    status: "enabled",
    createdBy: "user_0003",
    slug: "OVpYqHQZfR1u69PV",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.775Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.775Z"),
  },

  /* 2 */
  {
    _id: "5de931dd53674b6086cb80f2",
    isDeleted: false,
    alias: "ibk",
    name: "Interbank",
    status: "enabled",
    createdBy: "user_0003",
    slug: "zBjED2mhgU8bRiQ0",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.776Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.776Z"),
  },

  /* 3 */
  {
    _id: "5de931dd53674b6086cb80f3",
    isDeleted: false,
    alias: "bbva",
    name: "BBVA",
    status: "enabled",
    createdBy: "user_0003",
    slug: "J99DV82ui7KxK3XK",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.776Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.776Z"),
  },

  /* 4 */
  {
    _id: "5de931dd53674b6086cb80f4",
    isDeleted: false,
    alias: "scotia",
    name: "Scotiabank",
    status: "enabled",
    createdBy: "user_0003",
    slug: "enzRvclc2d4nX8Ea",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.777Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.777Z"),
  },

  /* 5 */
  {
    _id: "5de931dd53674b6086cb80f5",
    isDeleted: false,
    alias: "mi-banco",
    name: "Mibanco",
    status: "enabled",
    createdBy: "user_0003",
    slug: "ACb4j7Pmj9SOX6Yo",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.777Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.777Z"),
  },
];

export const updBanksCollection = [
  {
    _id: "5de931dd53674b6086cb80f1",
    isDeleted: false,
    alias: "bcp",
    name: "Banco de Crédito del Perú",
    status: "enabled",
    createdBy: "user_0003",
    slug: "OVpYqHQZfR1u69PV",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.775Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.775Z"),
  },

  /* 2 */
  {
    _id: "5de931dd53674b6086cb80f2",
    isDeleted: false,
    alias: "ibk",
    name: "Interbank",
    status: "enabled",
    createdBy: "user_0003",
    slug: "zBjED2mhgU8bRiQ0",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.776Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.776Z"),
  },

  /* 3 */
  {
    _id: "5de931dd53674b6086cb80f3",
    isDeleted: false,
    alias: "bbva",
    name: "BBVA",
    status: "enabled",
    createdBy: "user_0003",
    slug: "J99DV82ui7KxK3XK",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.776Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.776Z"),
  },

  /* 4 */
  {
    _id: "5de931dd53674b6086cb80f4",
    isDeleted: false,
    alias: "scotia",
    name: "Scotiabank",
    status: "enabled",
    createdBy: "user_0003",
    slug: "enzRvclc2d4nX8Ea",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.777Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.777Z"),
  },

  /* 5 */
  {
    _id: "5de931dd53674b6086cb80f5",
    isDeleted: false,
    alias: "mi-banco",
    name: "Mibanco",
    status: "enabled",
    createdBy: "user_0003",
    slug: "ACb4j7Pmj9SOX6Yo",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.777Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.777Z"),
  },

  /* 6 */
  {
    _id: "5de931dd53674b6086cb80f6",
    isDeleted: false,
    alias: "GNB",
    name: "GNB",
    status: "enabled",
    createdBy: "user_0003",
    slug: "ACb4j7Pmj9S61hn",
    __v: 0,
    createdAt: ISODate("2019-12-05T16:35:41.777Z"),
    updatedAt: ISODate("2019-12-05T16:35:41.777Z"),
  },
];
