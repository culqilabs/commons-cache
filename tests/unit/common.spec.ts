import * as commonsService from "../../src/services/common";
import * as dataAccess from "../../src/services/data-access";
import { BanksCollection } from "./commonsData";

describe("Pass Test", () => {
  it("True equals True", async () => {
    expect(true).toBeTruthy();
  });
});

/*
describe("common-cache test", () => {
  describe("getBySlug", () => {
    let common: any;
    it("should allow getting a value by slug id only", async () => {
      common = await commonsService.getBySlug({ collection: "banks", detail: { slug: "zBjED2mhgU8bRiQ0" } });
      expect(common).toBeTruthy();
      expect(common).not.toHaveProperty("length");
    });

    it("should return a 'record not found error'", async () => {
      const inexistantCommon = await commonsService.getBySlug({ collection: "banks", detail: { slug: "inexistant" } });
      expect(inexistantCommon).toHaveProperty("error");
      expect(inexistantCommon.error.message).toEqual("generic.errors.recordNotFound");
    });
  });

  describe("list", () => {
    let commonsList: any;
    it("should allow getting an entire collection only", async () => {
      const result = await commonsService.list("banks");
      commonsList = result.data;
      expect(commonsList).toBeTruthy();
      expect(commonsList).toHaveProperty("length");
    });

    it("should return undefined when querying and inexistant collection", async () => {
      const result = await commonsService.list("inexistant");
      commonsList = result.data;
      expect(commonsList).toBeFalsy();
    });
  });

  describe("getByName", () => {
    let common: any;
    it("should allow getting a value by name only", async () => {
      common = await commonsService.getByName({ collection: "banks", detail: { name: "Interbank" } });
      expect(common).toBeTruthy();
      expect(common).not.toHaveProperty("length");
    });

    it("should return a 'record not found error'", async () => {
      const inexistantCommon = await commonsService.getByName({ collection: "banks", detail: { name: "inexistant" } });
      expect(inexistantCommon).toHaveProperty("error");
      expect(inexistantCommon.error.message).toEqual("generic.errors.recordNotFound");
    });
  });

  describe("getByAlias", () => {
    let common: any;
    it("should allow getting a value by alias", async () => {
      common = await commonsService.getByAlias({ collection: "banks", detail: { alias: "bcp" } });
      expect(common).toBeTruthy();
      expect(common).not.toHaveProperty("length");
    });

    it("should return a 'record not found error'", async () => {
      const inexistantCommon = await commonsService.getByAlias({ collection: "banks", detail: { alias: "inexistant" } });
      expect(inexistantCommon).toHaveProperty("error");
      expect(inexistantCommon.error.message).toEqual("generic.errors.recordNotFound");
    });
  });
});
*/
